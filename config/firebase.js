const admin = require('firebase-admin');

const serviceAccount = require('./myproject-cbb4b-firebase-adminsdk-zj6oi-ac59842705.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

module.exports = db;