const router = require("express").Router();
const TodolistService = require("../service/Todolist.service");

router.get("/", async (req, res) => {
    try{

        const user_id = req.headers.user_id;
        //const result = await TodolistService.getTodolist(user_id);
        const result ={ 
            "user_id" : "2576112",
            "user_name" : "hi",
            "items" : [
                {
                    "id" : "1",
                    "todo_title" : "할일 1",
                    "todo_done" : false,
                    "createdAt" : null,
                    "modifiedAt" : null,
                    "dDay" : null
                },
                 {
                    "id" : "2",
                    "todo_title" : "할일 2",
                    "todo_done" : false,
                    "createdAt" : null,
                    "modifiedAt" : null,
                    "dDay" : null
                },
                 {
                    "id" : "3",
                    "todo_title" : "할일 3",
                    "todo_done" : false,
                    "createdAt" : null,
                    "modifiedAt" : null,
                    "dDay" : null
                }
            ]
        };
        res.status(200).send(result);
    }catch(error){
        res.status(500).send(error.message);
    }
});

router.post("/", async (req, res) => {
    try{
        const todolist = req.body;
        const result = await TodolistService.updateTodolist(todolist);

        res.status(200).send(result);
    }catch(error){
        res.status(500).send(error.message);
    }
});


// router.post("/conversation", async(req, res) => {
//     try {
//         const user_id = req.body.user_id;
//         const result = await TodolistService.openConversation(user_id);
//     } catch (error) {
//         res.status(500).send(error.message);
//     }
// })


router.post("/message", async(req, res) => {
    try {
        const user_id = req.body.user_id;
        const resultConversation = await TodolistService.openConversation(user_id);

        const conversation_id = resultConversation.data.conversation.id;
        const resultMessage = await TodolistService.sendMessage(conversation_id);
        res.status(200).send(resultMessage.data);
    } catch (error) {
        res.status(500).send(error.message);
    }
})
module.exports = router;